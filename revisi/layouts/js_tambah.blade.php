<script type="text/javascript">
  $('.date').datepicker({  
    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
  });  
</script>
<script type="text/javascript">
  function angka(e) {
  if (!/^[0-9]+$/.test(e.value)) {
    e.value = e.value.substring(0,e.value.length-1);
  }
}
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/js/select2.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){  
    $("#pertama").on("change", function(){
      var stok = $("#pertama option:selected").attr("stok");
      $(".stok").val(stok);
      var harga = $("#pertama option:selected").attr("harga");
      $("#harga").val(harga);
      var text = $("#pertama option:selected").val();
      $(".nama_barang").val(text);
      
    }); 

    $(".select22").select2({
      placeholder: "Nama Barang",
      allowClear: true
    });
    var i=1;
    $('.tambah').click(function(){  
      i++;
      $('tbody').append(
        '<tr id="row'+i+'">'
        +
        '<td width="40%"><select style="width: 100%;" class="form-control select22 mendua" id="'+i+'" name="id_barang[]"><option></option>@foreach($barang as $barangs)<option value="{{$barangs->id_barang}}" stok2="{{$barangs->stok}}" harga2="{{number_format($barangs->harga)}}">{{$barangs->nama_barang}}</option>@endforeach</select><input type="hidden" class="nama_barang'+i+'" value=""></td>'
        +
        '<td width="15%"><input style="width: 100%;" type="text" readonly name="stok[]" class="form-control mb-2 stok2'+i+'"></td>'
        +
        '<td width="20%"><input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" id="inlineFormInput" placeholder="Jumlah" onkeyup="angka(this);" required=""></td>'  
        
        +'<td width="15%"><input style="width: 100%;" type="text" readonly name="harga[]" class="form-control mb-2 harga2'+i+'"></td>'
        
        +
        '<td><a href="#" class="btn btn-sm btn-danger mb-2 remove" id="'+i+'"><i class="fas fa-minus"></i></a></td>'
        +
        '</tr>'
        );
      $(".select22").select2({
        placeholder: "Nama Barang",
        allowClear: true
      });
      $(document).on('click', '.remove', function(){  
        var button_id = $(this).attr("id");   
        $('#row'+button_id+'').remove();  
      });  
      $(document).on('change', '.mendua', function(){  
        var select_id = $(this).attr("id"); 
        var stok2 = $('#'+select_id+' option:selected').attr('stok2');
        $('.stok2'+select_id+'').val(stok2);
        var harga2 = $('#'+select_id+' option:selected').attr('harga2');
        $('.harga2'+select_id+'').val(harga2);
        var texts = $('#'+select_id+' option:selected').val();
        $('.nama_barang'+select_id+'').val(texts);
        s = select_id - 1;
        se = select_id - 2;
        sel = select_id - 3;
        sele = select_id - 4;
        selec = select_id - 5;
        select = select_id - 6;
        selecte = select_id - 7;
        selected = select_id - 8;
        selecteda = select_id - 9;
        selectedab = select_id - 10;
        if ($(".nama_barang").val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }else if ($('.nama_barang'+s+'').val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }else if ($('.nama_barang'+se+'').val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }else if ($('.nama_barang'+sel+'').val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }else if ($('.nama_barang'+sele+'').val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }else if ($('.nama_barang'+selec+'').val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }else if ($('.nama_barang'+select+'').val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }else if ($('.nama_barang'+selecte+'').val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }else if ($('.nama_barang'+selected+'').val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }else if ($('.nama_barang'+selecteda+'').val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }else if ($('.nama_barang'+selectedab+'').val() == $('.nama_barang'+select_id+'').val()) {
          Swal.fire({
            type: 'error',
            title: 'Barang Yang Sudah Dipilih Tidak Dapat Dipilih Kembali!',
            text: 'Error! Silahkan Pilih Kembali'
          })
        }
      });
    });
  });
</script>