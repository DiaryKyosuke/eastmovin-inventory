@extends('layouts.inventorymasuk')
@section('title','Form Inventory Barang Masuk')
@section('masuk')
<div class="container">
	@if(count($errors) > 0)
	<div class="alert alert-danger">
		<strong>Whoops!</strong> Inputkankan Data Anda.<br>
		<ul>
			@foreach($errors as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<h2>Form Inventory Barang Masuk</h2>
	<br>
	<div class="row">
		<form action="{{url('inventory/masuk/store')}}" method="post" enctype="multipart/form-data">
			@csrf
			<div class="form-row align-items-center">
				<div class="col-auto">
					<div class="col-md-12">
						<strong>Tanggal </strong>
					</div>
				</div>
				<div class="col-auto">
					<div class="col-md-12">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="la la-calendar glyphicon-th"></i></div>
							</div>
							<input type="text" class="date form-control" name="tanggal" placeholder="Tanggal">
							<!-- hidden id_transaksi dan jenis -->
							<input type="hidden" name="jenis" value="1">
							
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="col-md-12">
				<div class="form-group row">
					<strong class="col-sm-2 col-form-label">Keterangan</strong>
					<div class="col-md-10">
						<textarea class="mx-sm-3" rows="3" name="keterangan"></textarea>
					</div>
				</div>
			</div>
			<br>


			<div class="col-md-12">
				<table class="table table-borderless" width="100%"> 
					<thead>
						<tr>
							<td><strong>Barang</strong></td>
							<td><strong>Stok</strong></td>
							<td><strong>Jumlah</strong></td>
						</tr>
					</thead>
					<tbody>
						<tr>  
							<td width="40%">
								<select style="width: 100%;" class="form-control select22" name="id_barang[]" id="element">
									<option></option>
									@foreach($barang as $barangs)
									<option value="{{$barangs->id_barang}}" harga="{{$barangs->harga}}" stok="{{$barangs->stok}}">{{$barangs->nama_barang}}</option>
									@endforeach
									<input type="hidden" name="harga" id="harga">
								</select>
							</td> 
							<td width="15%">
								<input style="width: 100%;" type="text" name="stok[]" class="form-control mb-2 stok">
							</td>
							<td width="18%">
								<input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" id="inlineFormInput" placeholder="Jumlah">
							</td>

							<td>
								<a href="#" class="btn btn-sm btn-info mb-2 tambah"><i class="la la-plus"></i></a>
							</td>  
						</tr>  
					</tbody>
				</table>  
			</div>

			<div class="col-md-12">
				<br>
				<a href="{{ url('inventory.masuk.grid') }}" class="btn btn-sm btn-success">Back</a>
				<button type="submit" class="btn btn-sm btn-primary" >Simpan</button>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$('.date').datepicker({  

		format: 'dd-mm-yyyy',
		autoclose: true,
		todayHighlight: true
	});  
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/js/select2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){  
		$("#element").on("change", function(){
			var harga = $("#element option:selected").attr("harga");
			var stok = $("#element option:selected").attr("stok");
			$("#harga").val(harga);
			$(".stok").val(stok);
		}); 

		$(".select22").select2({
			placeholder: "Nama Barang",
			allowClear: true
		});
		var i=1;
		$('.tambah').click(function(){  
			i++;
			$('tbody').append(
				'<tr id="row'+i+'">'+

				'<td width="40%"><select style="width: 100%;" class="form-control select22 kedua" id="'+i+'" name="id_barang[]"><option></option>@foreach($barang as $barangs)<option value="{{$barangs->id_barang}}" stoks="{{$barangs->stok}}">{{$barangs->nama_barang}}</option>@endforeach</select></td>'+

				'<td width="15%"><input style="width: 100%;" type="text" readonly name="stok[]" class="form-control mb-2 stoks'+i+'"></td>'+

				'<td width="18%"><input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" id="inlineFormInput" placeholder="Jumlah"></td>'+

				'<td><a href="#" class="btn btn-sm btn-danger mb-2 remove" id="'+i+'"><i class="fas fa-minus"></i></a></td>'+
				'</tr>'
				);
			$(".select22").select2({
				placeholder: "Nama Barang",
				allowClear: true
			});
			$(document).on('click', '.remove', function(){  
				var button_id = $(this).attr("id");   
				$('#row'+button_id+'').remove();  
			});  
			$(document).on('change', '.kedua', function(){  
				var select_id = $(this).attr("id"); 
				var stoks = $('#'+select_id+' option:selected').attr('stoks');
				$('.stoks'+select_id+'').val(stoks);
			});
		});
	});
</script>
@endsection
