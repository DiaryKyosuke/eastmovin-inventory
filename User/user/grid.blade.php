@extends('layouts.masteruser')
@section('title','Data User')
@section('masteruser')
<div class="m-portlet m-portlet--mobile" style="width: 100%;">
		@if ($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{$message}}</p>
	</div>
	@endif
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data User
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{url('master.user.form')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
						<span>
					<i class="la la-plus"></i>
					<span>Tambah User</span>
				</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_2">
			<thead >
				<tr class="text-center">
					<th width="5%">No</th>
					<th>Usrname</th>
					<th>Email</th>
					<th>Role</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>
					<td>{{++$i}}.</td>
					<td>{{$user->username}}</td>
					<td>{{$user->email}}</td>
					<td class="text-center">{{$user->role}}</td>					
				<td class="text-center">{{$user->status}}</td>
					<td class="text-center">
					<a href="master.user.{{$user->id_user}}.edit_user" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>

					<a href="master/user/{{$user->id_user}}/destroy" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-trash"></i></a>
				</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@endsection