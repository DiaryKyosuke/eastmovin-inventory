@extends('layouts.inventorykeluar')
@section('title','Form Inventory Barang Keluar')
@section('keluar')
<div class="container">
	<h2>Form Inventory Barang Keluar</h2>
	<br>
	<div class="row">
		<form action="" method="post" enctype="multipart/form-data">

			<div class="form-row align-items-center">
				<div class="col-auto">
					<div class="col-md-12">
						<strong>Tanggal </strong>
					</div>
				</div>
				<div class="col-auto">
					<div class="col-md-12">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="la la-calendar glyphicon-th"></i></div>
							</div>
							<input type="text" class="date form-control" placeholder="Tanggal">
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="col-md-12">
				<div class="form-group row">
					<strong class="col-sm-2 col-form-label">Keterangan</strong>
					<div class="col-md-10">
						<textarea class="mx-sm-3" rows="3"></textarea>
					</div>
				</div>
			</div>
			<br>
			<div class="col-md-12">
				<div class="form-row align-items-center">
					<div class="col-md-4">
						<strong>Barang </strong>
					</div>
					<div class="col-md-2">
						<strong>Stok</strong>
					</div>
					<div class="col-md-2">
						<strong>Jumlah</strong>
					</div>
					<div class="col"></div>
				</div>
			</div>


			<div class="col-md-12">
				<div class="form-row align-items-center">
					<div class="col-md-4">
						<select id="barang" name="" class="form-control">
							<option></option>
							<option value="1">Tas</option>
							<option value="2">Rakia Pouch</option>
						</select>
					</div>
					<div class="col-md-2">
						<input type="text"  class="form-control mb-2" id="inlineFormInput">
					</div>
					<div class="col-md-2">
						<input type="text"  class="form-control mb-2" id="inlineFormInput" placeholder="Jumlah">
					</div>
					<div class="col">
						<button type="submit" class="btn btn-sm btn-info mb-2"><i class="la la-plus"></i></button>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<br>
				<a href="{{ url('inventory.keluar.grid') }}" class="btn btn-sm btn-success">Back</a>
				<button type="submit" class="btn btn-sm btn-primary">Simpan</button>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">

	$('.date').datepicker({  

		format: 'dd-mm-yyyy',
		autoclose: true,
		todayHighlight: true

	});  
</script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/js/select2.min.js"></script>



<script type="text/javascript">
	$("#barang").select2({
		placeholder: "Nama Barang",
		allowClear: true
	});
</script>
@endsection