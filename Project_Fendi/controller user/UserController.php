<?php

namespace App\Http\Controllers\master;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pengguna;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Pengguna::latest()->paginate(5);
        return view('master/user/grid', compact('users'))
        ->with('i',(request()->input('page',1) -1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required',
            'role' => 'required',
            'status' => 'required'
        ]);
        $do = new Pengguna($request->all());
        $do->save();
        return redirect('master.user.grid')
        ->with('success','Data baru berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id_user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id_user)
    {
        $users = Pengguna::find($id_user);   
        return view('master.user.edit_user',compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_user)
    {
       $this->validate($request, [
        'username' => 'required',
        'email' => 'required',
        'role' => 'required',
        'status' => 'required'
    ]);
       $users = Pengguna::find($id_user);
       $users->username = $request->get('username');
       $users->email = $request->get('email');
       $users->role = $request->get('role');
       $users->status = $request->get('status');
       $users->save();
       return redirect('master.user.grid')
       ->with('success','Data berhasil diupdate');
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_user)
    {
        $user = Pengguna::find($id_user);
        $user->delete();
        return redirect('master.user.grid')
        ->with('success','Data berhasil dihapus');
    }
}
