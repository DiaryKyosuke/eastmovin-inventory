@extends('layouts.masterbarang')
@section('title','Data Barang')
@section('masterbarang')
<div class="m-portlet m-portlet--mobile" style="width: 100%;">
	@if(session('sukses'))
	<div class="alert alert-success text-center" role="alert">
		{{session('sukses')}}
	</div>
	@endif
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data Barang
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{url('master.barang.form')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>Tambah Barang</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_barang">
			<thead >
				<tr class="text-center">
					<th width="5%">No</th>
					<th>Nama Barang</th>
					<th>Harga</th>
					<th width="120px;">Aksi</th>
				</tr>
			</thead>
			<tbody>
				@foreach($data_barang as $barang)
				<tr>
					<td>{{++$no}}.</td>
					<td>{{$barang->nama_barang}}</td>
					<td>Rp.{{$barang->harga}}</td>
					<td class="text-center">
						<a href="master.barang.{{$barang->id_barang}}.edit_barang" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>
						
						<a href="master.barang.{{$barang->id_barang}}.grid" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus')"><i class="fa fa-trash"></i></a>

						<a  data-fancybox href="{{ url('uploadgambar') }}/{{ $barang->gambar }}" class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-camera-retro"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection