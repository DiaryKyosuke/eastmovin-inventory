@extends('layouts.masterbarang')
@section('title','Form Edit Barang')
@section('masterbarang')
<div class="col-lg-12">
	@if(count($errors) > 0)
	<div class="alert alert-danger">
		<strong>Whoops!</strong> Inputkankan Data Anda.<br>
		<ul>
			@foreach($errors as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Form Barang
					</h3>
				</div>
			</div>
		</div>

		<!--begin::Form-->
		<form class="m-form m-form--label-align-right" action="master.barang.{{$barang->id_barang}}.update" method="post" enctype="multipart/form-data" autocomplete="off">
			{{csrf_field()}}
			<div class="m-portlet__body">
				<div class="m-form__section m-form__section--first">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Nama Barang:</label>
						<div class="col-lg-6">
							<input type="text" name="nama_barang" class="form-control m-input" placeholder="Nama Barang" value="{{$barang->nama_barang}}">
							<span class="m-form__help">Masukkan Nama Barang</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Harga:</label>
						<div class="col-lg-6">
							<input type="text" name="harga" class="form-control m-input" placeholder="Harga Barang" value="{{$barang->harga}}" data-mask="000.000.000" data-mask-reverse="true">
							<span class="m-form__help">Masukkan Harga Barang</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Foto Barang:</label>
						<div class="col-lg-6">
							<div class="custom-file">
								<input type="file" name="gambar" class="custom-file-input" id="inputGroupFile02">
								<label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02" id="file">{{$barang->gambar}}</label>
							</div>
							<span class="m-form__help">Masukkan Foto Barang</span>
						</div>
					</div>
					<div class="form-group m-form__group row" id="only-number">
						<label class="col-lg-2 col-form-label">Stok Barang :</label>
						<div class="col-lg-6">
							<input type="text" id="number" name="stok" class="form-control m-input" placeholder="Stok Barang" readonly="readonly" value="{{$barang->stok}}">
							<span class="m-form__help">Masukkan Jumlah Stok Barang</span>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-6">
							<button type="submit" class="btn btn-primary">Update</button>
							<a href="{{url('master.barang.grid')}}" class="btn btn-secondary">Batal</a>
						</div>
					</div>
				</div>
			</div>
		</form>

		<!--end::Form-->
	</div>
	<!--end::Portlet-->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var wrapper = $('<label/>').css({height:0,width:0,'overflow':'hidden'});
		var fileInput = $(':file').wrap(wrapper);
		var text;

		fileInput.change(function(){
			$this = $(this);
			$('#file').html($this.val());
			text = $('#file').html();
			text = text.substring(text.lastIndexOf("\\") + 1, text.length);
			$('#file').html(text);
		});
	});
	// inputan Hanya Angka \\
	 $(function() {
      $('#only-number').on('keydown', '#number', function(e){
          -1!==$
          .inArray(e.keyCode,[46,8,9,27,13,110,190]) || /65|67|86|88/
          .test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey)
          || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey|| 48 > e.keyCode || 57 < e.keyCode)
          && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
      });
    })
</script>
@endsection							