<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route Dashboard \\
Route::get('/dasboard.aplikasi','aplikasiController@index');

// Route Barang \\
Route::get('master.barang.grid', 'master\BarangController@barang');
Route::get('master.barang.form', 'master\BarangController@form');
Route::post('master.barang.form', 'master\BarangController@tambah');
Route::get('master.barang.{id_barang}.edit_barang', 'master\BarangController@edit');
Route::post('master.barang.{id_barang}.update', 'master\BarangController@update');
Route::get('master.barang.{id_barang}.grid','master\BarangController@delete');

// Route User \\
Route::get('master.user.grid', 'master\UserController@user');
Route::get('master.user.form', 'master\UserController@form');
Route::get('master.user.edit_user', 'master\UserController@edit');

// inventory barang masuk dan keluar
Route::get('inventory.masuk.grid','inventory\masukController@index');
Route::get('inventory.masuk.form','inventory\masukController@create');

Route::get('inventory.keluar.grid','inventory\keluarController@index');
Route::get('inventory.keluar.form','inventory\keluarController@create');
