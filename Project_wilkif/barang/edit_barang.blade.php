@extends('layouts.masterbarang')
@section('title','Form Edit Barang')
@section('masterbarang')
<div class="col-lg-12">
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Form Barang
					</h3>
				</div>
			</div>
		</div>

		<!--begin::Form-->
		<form class="m-form m-form--label-align-right" action="master.barang.{{$barang->id_barang}}.update" method="post" enctype="multipart/form-data">
		{{csrf_field()}}
			<div class="m-portlet__body">
				<div class="m-form__section m-form__section--first">
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Nama Barang:</label>
						<div class="col-lg-6">
							<input type="text" name="nama_barang" class="form-control m-input" placeholder="Nama Barang" value="{{$barang->nama_barang}}">
							<span class="m-form__help">Masukkan Nama Barang</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Harga:</label>
						<div class="col-lg-6">
							<input type="text" name="harga" class="form-control m-input" placeholder="Harga Barang" value="{{$barang->harga}}">
							<span class="m-form__help">Masukkan Harga Barang</span>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Foto Barang:</label>
						<div class="col-lg-6">
							<input type="file" name="gambar" class="form-control m-input" value="{{$barang->gambar}}">
							<span class="m-form__help">Masukkan Foto Barang</span>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-6">
							<button type="submit" class="btn btn-primary">Tambah</button>
							<a href="{{url('master.barang.grid')}}" class="btn btn-secondary">Batal</a>
						</div>
					</div>
				</div>
			</div>
		</form>

		<!--end::Form-->
	</div>
	<!--end::Portlet-->
</div>
@endsection							