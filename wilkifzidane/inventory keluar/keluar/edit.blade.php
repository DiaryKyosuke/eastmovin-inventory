@extends('layouts.inventorykeluar')
@section('title','Form Inventory Barang Keluar')
@section('keluar')
<div class="container">
  @if(count($errors) > 0)
  <script type="text/javascript">
    $(document).ready(function(){
      Swal({
        type: 'error',
        title: 'Maaf... Masukkan Data Dengan Benar !!!',
        text: 'Data Tidak Boleh Di Kosongkan ',
      })
    });
  </script>
  @endif
  <div class="alert-warning m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert" style="border-radius: 20px;">
    <div class="m-alert__icon">
      <i class="flaticon-signs-2 text-danger"></i>
    </div>
    <div class="m-alert__text text-danger">
      Sebelum Data Di Simpan Pastikan Inputan Data Nama Barang Tidak Boleh Ada Yang Sama... !!
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-lg-10">
      <div class="m-portlet">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
              </span>
              <h3 class="m-portlet__head-text">
                Form Tambah Data Barang Keluar
              </h3>
            </div>
          </div>
        </div>

        <!--begin::Form-->
        <form action="inventory.keluar.{{$transaksi->id_transaksi}}.update" method="post" enctype="multipart/form-data" autocomplete="off" class="m-form m-form--label-align-right">
          @csrf
          <div class="m-portlet__body">
            <div class="m-form__section m-form__section--first">
              <div class="form-row align-items-center">
                <div class="col-auto">
                  <div class="col-md-12">
                    <strong>Tanggal </strong>
                  </div>
                </div>
                <div class="col-auto">
                  <div class="col-md-12">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text"><i class="la la-calendar glyphicon-th"></i></div>
                      </div>
                      <input type="text" class="date form-control" name="tanggal" autocomplete="off" placeholder="Tanggal"value="{{Carbon\Carbon::parse($transaksi->tanggal)->formatLocalized('%d-%m-%Y')}}">
                      <!-- hidden id_transaksi dan jenis -->
                      <input type="hidden" name="jenis" value="0">
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <div class="form-row align-items-center">
                <div class="col-auto">
                  <div class="col-md-12">
                    <strong>Keterangan </strong>
                  </div>
                </div>
                <div class="col-auto">
                  <div class="col-md-12">
                    <div class="input-group">
                      <textarea  rows="3" name="keterangan">{{$transaksi->keterangan}}</textarea>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <input type="hidden" name="id_transaksi[]" value="{{$details->id_transaksi}}">
              <input type="hidden" name="hak_akses" value="buka">
              <br>
              <div class="col-md-12">
                <table class="table table-borderless" width="100%"> 
                  <thead>
                    <tr>
                      <td><strong>Barang</strong></td>
                      <td><strong>Stok</strong></td>
                      <td><strong>Jumlah</strong></td>
                      <td><strong>Harga</strong></td>
                      <td>
                        <a href="#" class="btn btn-sm btn-info mb-2 tambah"><i class="la  la-plus"></i></a>
                      </td> 
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($id_transaksi as $id_trans)
                    <tr id="apus{{++$i}}">  
                      <td width="40%">
                        <select style="width: 100%;" class="form-control select22" name="id_barang[]" id="pertama{{$i}}">
                          <option></option>
                          @foreach($nama_barang as $barangs)
                          @if(($barangs->id_barang == $id_trans->id_barang))
                          <option value="{{$barangs->id_barang}}" stok{{$i}}="{{$barangs->stok}}" jumlah{{$i}}="{{$id_trans->jumlah}}" harga{{$i}}="{{number_format($barangs->harga)}}" selected="true">
                            {{$barangs->nama_barang}}
                          </option>
                          @else
                          <option value="{{$barangs->id_barang}}" stok{{$i}}="{{$barangs->stok}}" jumlah{{$i}}="{{$barangs->jumlah}}" harga{{$i}}="{{number_format($barangs->harga)}}">
                            {{$barangs->nama_barang}}
                          </option>
                          @endif
                          @endforeach
                        </select>
                        <input style="width: 100%;" name="id[]" type="hidden" readonly class="form-control mb-2" value="{{$id_trans->id_barang}}">
                      </td> 
                      <td width="18%">
                        <input style="width: 100%;" name="stok[]" class="form-control mb-2" id="stok{{$i}}" placeholder="Stok" readonly="" value="{{$id_trans->stok}}">
                        <input type="hidden" name="stox[]" value="{{$id_trans->stok}}">
                      </td>
                      <td width="18%">
                        <input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" id="jumlah{{$i}}" placeholder="Jumlah" value="{{$id_trans->jumlah}}">

                        <input type="hidden" name="jums[]" value="1">
                        <input type="hidden" name="jum[]" value="{{$id_trans->jumlah}}">
                      </td>
                      <td>
                        <input style="width: 100%;" name="harga[]" class="form-control mb-2" id="harga{{$i}}" placeholder="Harga" readonly="" value="{{number_format($id_trans->harga)}}">
                      </td>

                      <td>
                       <a href="#" id="gosok{{$i}}" class="btn btn-sm btn-danger mb-2 remove"><i class="fas fa-minus"></i></a>
                     </td>  
                   </tr>
                   @endforeach 
                 </tbody>
               </table>  
             </div>
           </div>
         </div>
         <div class="m-portlet__foot m-portlet__foot--fit">
          <div class="m-form__actions m-form__actions">
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-6">
                <button type="submit" class="btn btn-sm btn-primary" >Simpan</button>
                <a href="{{ url('inventory.keluar.grid') }}" class="btn btn-sm btn-success">Back</a>
              </div>
            </div>
          </div>
        </div>
      </form>

      <!--end::Form-->
    </div>
    <!--end::Portlet-->
  </div>
</div>
</div>
@include('layouts.js')
@endsection
