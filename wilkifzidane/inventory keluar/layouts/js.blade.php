<script type="text/javascript">
  $('.date').datepicker({  

    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
  });  
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/js/select2.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){  
    var i=1;
    $('.tambah').click(function(){  
      i++;
      $('tbody').append(
        '<tr id="row'+i+'">'
        +
        '<td width="40%"><select style="width: 100%;" class="form-control select22 mendua" id="'+i+'" name="id_barang[]"><option></option>@foreach($nama_barang as $barangs)<option value="{{$barangs->id_barang}}"stok2="{{$barangs->stok}}" harga2="{{number_format($barangs->harga)}}">{{$barangs->nama_barang}}</option>@endforeach</select></td>'
        +
        '<input style="width: 100%;" name="id[]" type="hidden" readonly class="form-control mb-2">'+
        '</td>'+

        '<td width="15%"><input style="width: 100%;" type="text" readonly name="stok[]" class="form-control mb-2 stok2'+i+'"></td>'
        + 
        '<input type="hidden" name="stox[]" class="form-control mb-2">'
        +
        '<td width="20%"><input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" id="inlineFormInput" placeholder="Jumlah">'
        +
        '<input style="width: 100%;" type="hidden" name="jums[]" class="form-control mb-2"  value="0"></td>'
        +
        '<input style="width: 100%;" type="hidden" name="jum[]" readonly class="form-control mb-2">'
        +
        '<td width="15%"><input style="width: 100%;" type="text" readonly name="harga[]" class="form-control mb-2 harga2'+i+'"></td>'
        +
        '<td><a href="#" class="btn btn-sm btn-danger mb-2 remove" id="'+i+'"><i class="fas fa-minus"></i></a></td>'
        +
        '</tr>'
        );
      $(".select22").select2({
        placeholder: "Nama Barang",
        allowClear: true
      });
      $(document).on('click', '.remove', function(){  
        var button_id = $(this).attr("id");   
        $('#row'+button_id+'').remove();  
      }); 
      $('.mendua').on("change",function(){  
        var select_id = $(this).attr("id"); 
        var stok2 = $('#'+select_id+' option:selected').attr('stok2');
        $('.stok2'+select_id+'').val(stok2);
        var harga2 = $('#'+select_id+' option:selected').attr('harga2');
        $('.harga2'+select_id+'').val(harga2);
      }); 
    });
  });
</script>
<!-- End Js Append -->

<script type="text/javascript">
  $(document).ready(function(){
    $(".select22").select2({
      placeholder: "Nama Barang",
      allowClear: true
    });
  <?php foreach ($id_transaksi as $key): ?>
        var select{{++$nol}}={{$satu++}}; 
        $('#pertama'+select{{$nol}}+'').on('change', function(){  
        var stok2 = $('#pertama'+select{{$nol}}+' option:selected').attr('stok'+select{{$nol}}+'');
        
        var jumlah = $('#pertama'+select{{$nol}}+' option:selected').attr('jumlah'+select{{$nol}}+'');

        var harga = $('#pertama'+select{{$nol}}+' option:selected').attr('harga'+select{{$nol}}+'');
        
        $('#stok'+select{{$nol}}+'').val(stok2);

        $('#jumlah'+select{{$nol}}+'').val(jumlah);

        $('#harga'+select{{$nol}}+'').val(harga);
      });
  <?php endforeach ?>

  <?php foreach ($id_transaksi as $key): ?>
    var hapus{{++$del}}={{$del1++}};
    $('#gosok'+hapus{{$del}}+'').on('click', function(){
      $('#apus'+hapus{{$del}}+'').remove();
    });
  <?php endforeach ?>

  });
</script>