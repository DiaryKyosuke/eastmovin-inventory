<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Barang;
use App\Charts\Chart2;
Use Carbon\Carbon;
use Alert;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
   public function __construct()
   {
    $this->middleware('auth');
}

public function index()
{
    alert()->success('Sukses Melakukan Login','Berhasil Login Aplikasi')->persistent('OK');
    return redirect('dasboard.aplikasi');
}

public function index2(Request $request)
{   
    // Mengambil Data Select Combobox berdasarkan Tahun
    $nggolek = $request->get('tahun');
    
    // Mengambil Tahun Sekarang
    $tauniki = date('Y');

    //Mengambil Nilai Data Perbulan 
    $transaksi = DB::select(
        'SELECT 
        extract(month from transaksi.tanggal) as month,
        count(*) as count
        FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"= 0
        GROUP BY
        1,extract(month from transaksi.tanggal)');

    if ($nggolek) {

        $jan = DB::select('SELECT extract(month from transaksi.tanggal) as month,
            count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"=0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
            GROUP BY
            1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 1');
        foreach ($jan as $key => $j) {}

            $feb = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"=0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 2');
        foreach ($feb as $key => $f) {}

            $mar = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 3');
        foreach ($mar as $key => $m) {}

            $apr = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 4');
        foreach ($apr as $key => $a) {}

            $mei = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 5');
        foreach ($mei as $key => $i) {}

            $jun = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 6');
        foreach ($jun as $key => $n) {}

            $jul = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 7');
        foreach ($jul as $key => $l) {}

            $ags = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 8');
        foreach ($ags as $key => $s) {}

            $sep = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 9');
        foreach ($sep as $key => $p) {}

            $okt = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 10');
        foreach ($okt as $key => $t) {}

            $nop = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 11');
        foreach ($nop as $key => $o) {}

            $des = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$nggolek.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 12');
        foreach ($des as $key => $e) {}
    } else {

        $jan = DB::select('SELECT extract(month from transaksi.tanggal) as month,
            count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"=0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
            GROUP BY
            1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 1');
        foreach ($jan as $key => $j) {}

            $feb = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"=0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 2');
        foreach ($feb as $key => $f) {}

            $mar = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 3');
        foreach ($mar as $key => $m) {}

            $apr = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 4');
        foreach ($apr as $key => $a) {}

            $mei = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 5');
        foreach ($mei as $key => $i) {}

            $jun = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 6');
        foreach ($jun as $key => $n) {}

            $jul = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 7');
        foreach ($jul as $key => $l) {}

            $ags = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 8');
        foreach ($ags as $key => $s) {}

            $sep = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 9');
        foreach ($sep as $key => $p) {}

            $okt = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 10');
        foreach ($okt as $key => $t) {}

            $nop = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi" where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 11');
        foreach ($nop as $key => $o) {}

            $des = DB::select('SELECT extract(month from transaksi.tanggal) as month,
                count(*) as count FROM "public"."transaksi" join "detail_transaksi" on detail_transaksi."id_transaksi" = transaksi."id_transaksi"  where transaksi."jenis"= 0 AND extract(year from transaksi.tanggal) = '.$tauniki.'
                GROUP BY
                1,extract(month from transaksi.tanggal) HAVING extract(month from transaksi.tanggal) = 12');
        foreach ($des as $key => $e) {}
    }
    
    foreach ($transaksi as $key => $value) 
    {
        if ($jan == null) {
            $jan = '';
        }else{
            $jan = $j->count;
        }

        if ($feb == null) {
            $feb = '';
        }else{
            $feb = $f->count;
        }

        if ($mar == null) {
            $mar = '';
        }else{
            $mar = $m->count;
        }

        if ($apr == null) {
            $apr = '';
        }else{
            $apr = $a->count;
        }

        if ($mei == null) {
            $mei = '';
        }else{
            $mei = $i->count;
        }
        
        if ($jun == null) {
            $jun = '';
        }else{
            $jun = $n->count;
        }

        if ($jul == null) {
            $jul = '';
        }else{
            $jul = $l->count;
        }

        if ($ags == null) {
            $ags = '';
        }else{
            $ags = $s->count;
        }
        
        if ($sep == null) {
            $sep = '';
        }else{
            $sep = $p->count;
        }
        
        if ($okt == null) {
            $okt = '';
        }else{
            $okt = $t->count;
        }

        if ($nop == null) {
            $nop = '';
        }else{
            $nop = $o->count;
        }

        if ($des == null) {
            $des = '';
        }else{
            $des = $e->count;
        }
        
        $bulans = [$jan,$feb,$mar,$apr,$mei,$jun,$jul,$ags,$sep,$okt,$nop,$des];
    }
    
    //Mengambil Jumlah Dan Jenis
    if ($nggolek) {
      $jumlah = DB::table('detail_transaksi')
        ->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
        ->join('transaksi','transaksi.id_transaksi','=','detail_transaksi.id_transaksi')
        ->where('transaksi.jenis','=',1)
        ->whereyear('transaksi.tanggal',$nggolek)
        ->get();
    } else {
        $jumlah = DB::table('detail_transaksi')
        ->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
        ->join('transaksi','transaksi.id_transaksi','=','detail_transaksi.id_transaksi')
        ->where('transaksi.jenis','=',1)
        ->whereyear('transaksi.tanggal',$tauniki)
        ->get();
    }
    
    //Perulangan Untuk Menampilkan Nama Barang
    $barang = Barang::all();
    $nama_barang = [];
    foreach ($barang as $key) {
        $nama_barang[] = $key->nama_barang;
    }
        return view('dasboard.aplikasi',compact('bulans','nama_barang','jumlah','nggolek'));
    }
}