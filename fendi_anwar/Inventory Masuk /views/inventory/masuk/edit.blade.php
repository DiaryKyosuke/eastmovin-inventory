@extends('layouts.inventorymasuk')
@section('title','Form Inventory Barang Masuk')
@section('masuk')
<div class="container">
	@include('layouts.alert_form_null')

	<div class="row justify-content-center">
		<div class="col-lg-10">
			<div class="m-portlet">
				<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30 alert-danger" role="alert">
					<div class="m-alert__icon">
						<i class="fas fa-exclamation-circle"></i>
					</div>
					<div class="m-alert__text">
						Sebelum Disimpan Pastikan Nama Barang Tidak Ada Yang Sama!!
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row justify-content-center">
		<div class="col-lg-10">
			<div class="m-portlet">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Form Edit Data Barang Masuk
							</h3>
						</div>
					</div>
				</div>

				<!--begin::Form-->
				<form action="inventory/masuk/{{$transaksis->id_transaksi}}/update" method="post" class="m-form m-form--label-align-right">
					@csrf
					<div class="m-portlet__body">
						<div class="m-form__section m-form__section--first">
							<div class="form-row align-items-center">
								<div class="col-auto">
									<div class="col-md-12">
										<strong>Tanggal </strong>
									</div>
								</div>
								<div class="col-auto">
									<div class="col-md-12">
										<div class="input-group">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="la la-calendar glyphicon-th"></i></div>
											</div>
											<input type="text" class="date form-control" name="tanggal" autocomplete="off" placeholder="Tanggal" value="{{ Carbon\Carbon::parse($transaksis->tanggal)->formatLocalized('%d-%m-%Y')}}">
											<!-- hidden jenis -->
											<input type="hidden" name="jenis" value="1">
											
										</div>
									</div>
								</div>
							</div>
							<br>

							<div class="form-row align-items-center">
								<div class="col-auto">
									<div class="col-md-12">
										<strong>Keterangan</strong>
									</div>
								</div>
								<div class="col-auto">
									<div class="col-md-12">
										<div class="input-group">
											<textarea class="mx-sm-3" rows="3" name="keterangan">{{$transaksis->keterangan}}</textarea>
										</div>
									</div>
								</div>
							</div>
							<br>
							<input type="hidden" name="id_transaksi[]" value="{{$details->id_transaksi}}">
							<input type="hidden" name="permission" value="{{$transaksis->permission}}">
							<div class="col-md-12">
								<table class="table table-borderless" width="100%"> 
									<thead>
										<tr>
											<td><strong>Barang</strong></td>
											<td><strong>Stok</strong></td>
											<td><strong>Jumlah</strong></td>
											<td><strong>Harga</strong></td>
											<td>
												<a href="#" class="btn btn-sm btn-info mb-2 tambah"><i class="la la-plus"></i></a>
											</td>
										</tr>
									</thead>
									<tbody>
										@foreach($dets as $deta)
										<tr id="baris{{++$i}}">
											<td width="40%">
												<select style="width: 100%;" class="form-control" name="id_barang[]" id="select{{$i}}">
													@foreach($bars as $barang)
													@if(($barang->id_barang == $deta->id_barang))
													<option value="{{$barang->id_barang}}" stoka{{$i}}="{{$barang->stok}}" jumlah{{$i}}="{{$deta->jumlah}}" harga{{$i}}="{{number_format($barang->harga)}}" selected="true">{{ $barang->nama_barang }}
													</option>
													@else
													<option value="{{$barang->id_barang}}" stoka{{$i}}="{{$barang->stok}}" jumlah{{$i}}="{{$barang->jumlah}}" harga{{$i}}="{{number_format($barang->harga)}}">{{ $barang->nama_barang }}
													</option>
													@endif 
													@endforeach
												</select>

												<input style="width: 100%;" name="id[]" type="hidden" readonly class="form-control mb-2" value="{{$deta->id_barang}}">
											</td>
											<td width="15%">
												<input style="width: 100%;" type="text" name="stok[]" readonly class="form-control mb-2" value="{{$deta->stok}}" id="stok{{$i}}">

												<input style="width: 100%;" type="hidden" name="stox[]" readonly class="form-control mb-2" value="{{$deta->stok}}">
											</td>
											<td width="18%"><input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" placeholder="Jumlah" id="jumlah{{$i}}" autocomplete="off" value="{{$deta->jumlah}}">


												<input style="width: 100%;" type="hidden" name="jums[]" class="form-control mb-2" value="1">
												<input style="width: 100%;" type="hidden" name="jum[]" readonly class="form-control mb-2" value="{{$deta->jumlah}}">
											</td>
											<td>
												<input type="text" name="harga[]" id="harga{{$i}}" value="{{number_format($deta->harga)}}" class="form-control mb-2" readonly>
											</td>
											<td>
												<a href="#" class="btn btn-sm btn-danger mb-2" id="del{{$i}}"><i class="fa fa-minus"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>  
							</div>
						</div>
					</div>

					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions">
							<div class="row">
								<div class="col-lg-2"></div>
								<div class="col-lg-6">
									<a href="{{ url('inventory.masuk.grid') }}" class="btn btn-sm btn-success">Back</a>
									<button type="submit" class="btn btn-sm btn-primary">Simpan</button>
								</div>
							</div>
						</div>
					</div>
				</form>

				<!--end::Form-->
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>
@include('layouts.js_edit_inventory_masuk')
@endsection
