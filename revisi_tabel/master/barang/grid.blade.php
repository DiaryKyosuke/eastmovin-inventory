@extends('layouts.masterbarang')
@section('title','Data Barang')
@section('masterbarang')
<div class="container">
	@if(session('sukses'))
	<div class="alert alert-success text-center" role="alert">
		{{session('sukses')}}
	</div>
	@endif
	<div class="row">
		<div class="col-md-12">
			<h1 style="text-align: center;">Data Master Barang</h1>
			<a href="{{url('master.barang.form')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air float-right">
				<span>
					<i class="la la-plus"></i>
					<span>Tambah Barang</span>
				</span>
			</a>

			<form action="/cari" method="get" class="form-inline my-2 my-lg-0" autocomplete="off">
				<label class="text-primary">Search:<input type="search" name="q" class="form-control form-control-sm" placeholder="" aria-controls="m_table_1"></label>
			</form>
		</div>
	</div>
	<table class="table table-bordered table-hover" width="100%">
		<thead class="thead-dark">
			<tr>
				<th scope="col" width="30px;">No</th>
				<th scope="col" class="text-center">Nama Barang</th>
				<th scope="col" class="text-center">Harga</th>
				<th scope="col" class="text-center">Foto</th>
				<th scope="col" width="120px;" class="text-center">Aksi</th>
			</tr>
		</thead>
		@foreach($data_barang as $barang)
		<tbody>
			<tr>
				<th>{{ ++ $no}}.</th>
				<td>{{$barang->nama_barang}}</td>
				<td>{{$barang->harga}}</td>
				<td>{{$barang->gambar}}</td>
				<td>
					<a href="master.barang.{{$barang->id_barang}}.edit_barang" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>
					<a href="master.barang.{{$barang->id_barang}}.grid" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus')"><i class="fa fa-trash"></i></a>
					<a href="#" class="btn btn-outline-success m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-camera-retro"></i></a>
				</td>
			</tr>
		</tbody>
		@endforeach
	</table>
	{!! $data_barang->links() !!}
</div>
@endsection