@extends('layouts.inventorymasuk')
@section('title','Inventory Barang Masuk')
@section('masuk')
<div class="m-portlet m-portlet--mobile" style="width: 100%;">
	@if(session('sukses'))
	<div class="alert alert-success text-center" role="alert">
		{{session('sukses')}}
	</div>
	@endif
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Inventory Barang Masuk
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{url('inventory.masuk.form')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>Tambah Data</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_masuk">
			<thead >
				<tr class="text-center">
					<th width="20px">No.</th>
					<th width="40%">Tanggal</th>
					<th width="40%">Keterangan</th>
					<th width="40%">Action</th>
				</tr>
			</thead>
			<tbody>
				
				<tr>
					<td>1.</td>
					<td>22/04/2001</td>
					<td>Terjual</td>
					<td class="text-center">
						<a class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="#"><i class="fas fa-id-card"></i></a>
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="#"><i class="fas fa-user-edit"></i></a>
						<a href="#" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air"><i class="fas fa-trash-alt"></i></a>
					</td>
				</tr>
				
			</tbody>
		</table>
	</div>
</div>
@endsection