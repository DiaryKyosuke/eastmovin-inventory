@extends('layouts.inventorymasuk')
@section('title','Form Inventory Barang Masuk')
@section('masuk')
<div class="container">
	<h2>Form Inventory Barang Masuk</h2>
	<br>
	<div class="row">
		<form action="" method="post" enctype="multipart/form-data">

			<div class="form-row align-items-center">
				<div class="col-auto">
					<div class="col-md-12">
						<strong>Tanggal </strong>
					</div>
				</div>
				<div class="col-auto">
					<div class="col-md-12">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="la la-calendar glyphicon-th"></i></div>
							</div>
							<input type="text" class="date form-control" placeholder="Tanggal">
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="col-md-12">
				<div class="form-group row">
					<strong class="col-sm-2 col-form-label">Keterangan</strong>
					<div class="col-md-10">
						<textarea class="mx-sm-3" rows="3"></textarea>
					</div>
				</div>
			</div>
			<br>


			<div class="col-md-12">
				<table class="table asd" width="100%">  
          <tr>
              <td><strong>Barang</strong></td>
              <td><strong>Stok</strong></td>
              <td><strong>Jumlah</strong></td>
          </tr>
          <tr>  
            <td width="40%">
              <select style="width: 100%;" name="" class="form-control select22">
                <option></option>
                <option value="1">Tas</option>
                <option value="2">Rakia Pouch</option>
              </select>
            </td> 
            <td width="15%">
              <input style="width: 100%;" type="text"  class="form-control mb-2" id="inlineFormInput">
            </td>
            <td width="18%">
              <input style="width: 100%;" type="text"  class="form-control mb-2" id="inlineFormInput" placeholder="Jumlah">
            </td>
            <td>
              <button type="button" class="btn btn-sm btn-info mb-2" id="addNewItem"><i class="la la-plus"></i></button>
            </td>  
          </tr>  
        </table>  
			</div>

			<div class="col-md-12">
				<br>
				<a href="{{ url('inventory.masuk.grid') }}" class="btn btn-sm btn-success">Back</a>
				<button type="submit" class="btn btn-sm btn-primary">Simpan</button>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">

	$('.date').datepicker({  

		format: 'dd-mm-yyyy',
		autoclose: true,
		todayHighlight: true

	});  
</script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.1/js/select2.min.js"></script>



<script type="text/javascript">
  $(".select22").select2({
    placeholder: "Nama Barang",
    allowClear: true
  });

  $(document).ready(function(){  
    var i=1;  
    $('#addNewItem').click(function(){  
     i++;  
     $('.asd').append('<tr id="row'+i+'"><td width="40%"><select style="width: 100%;" name="" class="form-control select2"><option></option><option value="1">Tas</option><option value="2">Rakia Pouch</option></select></td><td width="15%"><input style="width: 100%;" type="text"  class="form-control mb-2" id="inlineFormInput"></td><td width="18%"><input style="width: 100%;" type="text"  class="form-control mb-2" id="inlineFormInput" placeholder="Jumlah"></td><td><button type="button" class="btn btn-sm btn-danger mb-2 btn_remove" id="'+i+'"><i class="fas fa-minus"></i></button></td></tr> ');
     $('.select2').select2({
      placeholder: "Nama Barang",
      allowClear: true
    }); 
     $(document).on('click', '.btn_remove', function(){  
       var button_id = $(this).attr("id");   
       $('#row'+button_id+'').remove();  
     });    
   });
  });
</script>
@endsection