<?php

namespace App\Http\Controllers\inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\transaksi;
use App\detail;
use App\Barang;
use Alert;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class keluarController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
     $this->middleware('auth');
 }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
     $transaksi = DB:: table('transaksi')
     ->where('jenis','=',0)
     ->latest()
     ->paginate();

     $akses = DB::table('user')
    ->join('users','users.id','=','user.id_user')
    ->where('user.username','=',Auth::user()->username)
    ->first();
    $c = 1;
    $b = 0;
    return view('inventory/keluar/grid', compact('transaksi','akses','c','b'))
    ->with('i',(request()->input('page',1) -1)*5);
 }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response 
    */
    public function create()
    {
        $akses = DB::table('user')
                ->join('users','users.id','=','user.id_user')
                ->where('user.username','=',Auth::user()->username)
                ->first();
        $barang =\App\Barang::latest()->paginate();
        return view('inventory/keluar/form', ['barang' => $barang,'akses'=>$akses]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
     $this->validate($request,[
        'tanggal'      =>'required',
        'keterangan'   =>'required',
        'jenis'        =>'required',
        'id_barang'    =>'required',
        'harga'        =>'required',
        'jumlah'       =>'required'
    ]);
     $transaksi = transaksi::create([
        'tanggal' =>Carbon::parse($request->tanggal)->formatLocalized('%Y-%m-%d'),
        'keterangan' => request('keterangan'),
        'jenis' => request('jenis'),
        'permission' => request('hak_akses')
    ])->id_transaksi;

     if (count($request->id_barang) > 0 ){
        foreach ($request->id_barang as $item => $v) {
            $detail= array(
                $harga = $request->harga[$item],
                $harga_str = preg_replace("/[^0-9]/", "", $harga),
                'id_transaksi' => $transaksi,
                'id_barang'=> $request->id_barang[$item], 
                'harga' => $harga_str, 
                'jumlah'=> $request->jumlah[$item]
            );
            detail::create($detail);
        }
    }
  return redirect('inventory.keluar.grid')->with('sukses','Selamat Data Yang Anda Inputkan Berhasil Disimpan !!!');
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_transaksi)
    {
        $akses = DB::table('user')
                ->join('users','users.id','=','user.id_user')
                ->where('user.username','=',Auth::user()->username)
                ->first();
        $transaksi = transaksi::find($id_transaksi);
        $detail = DB::table('detail_transaksi')
        ->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
        ->where('id_transaksi','=',$id_transaksi)
        ->paginate();
        return view('inventory/keluar/view',compact('transaksi','detail','akses'))->with('i');   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_transaksi)
    {
     /*ini untuk tanggal dan keterangan*/
     $transaksis = transaksi::find($id_transaksi);
     /*yang ini untuk menampilkan jumlah dan harga*/
     $details = detail::find($id_transaksi);
     /*jumlah form sesuai dengan id_transaksi di detail_transaksi (perulangan)*/
     $dets = DB::table('detail_transaksi')
     ->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
     ->where('id_transaksi','=', $id_transaksi)->paginate();
     /*dibawah ini untuk menampilkan harga dan stok*/
     $barangs = Barang::find($details->id_barang);
     /*yang dibawah ini digunakan untuk perulangan di option select2*/
     $bars = Barang::find($details->id_barang)->paginate();
        // yang ini saya gunakan untuk membuat deklarasi sesuai nomor urut perulangan
     $b = 1;
     $c = 1;
     $d = 0;
     $hapus = 0;
     $terhapus = 1;
     $akses = DB::table('user')
                ->join('users','users.id','=','user.id_user')
                ->where('user.username','=',Auth::user()->username)
                ->first();
        // ini untuk select2 di append
     $benda =\App\Barang::latest()->paginate();
     return view('inventory.keluar.edit',compact('transaksis','details','barangs','bars', 'dets' ,'b','c','d','benda','hapus','terhapus','akses'))->with('i');
 }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_transaksi)
    {

    $transaksi = transaksi::create([
        'tanggal' =>Carbon::parse($request->tanggal)->formatLocalized('%Y-%m-%d'),
        'keterangan' => request('keterangan'),
        'jenis' => request('jenis'),
        'permission' => request('permission')
    ])->id_transaksi;

    if (count($request->id_barang) > 0 ){
        foreach ($request->id_barang as $item => $v) {
            $detail= array(
                $harga = $request->harga[$item],
                $harga_str = preg_replace("/[^0-9]/", "", $harga),
                'id_transaksi' => $transaksi,
                'id_barang'=> $request->id_barang[$item], 
                'harga'=> $harga_str, 
                'jumlah'=> $request->jumlah[$item]
            );
            detail::create($detail);
        }
    }

  $trans = transaksi::find($id_transaksi);
  $trans->delete();

  return redirect('inventory.keluar.grid')->with('sukses','Selamat Data Yang Anda Inputkan Berhasil Disimpan !!!');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_transaksi)
    {
        $transaksi = transaksi::find($id_transaksi);
        $transaksi->delete();
        return redirect('inventory.keluar.grid')->with('sukses','Selamat Data Telah Berhasil Dihapus !!!');
    }


    public function kunci(Request $request,$id_transaksi)
    {
        $this->validate($request,[
            'hak_akses' => 'required'
        ]);
        $transaksi = transaksi::find($id_transaksi);
        $transaksi->permission = $request->get('hak_akses');
        $transaksi->save();
        if (count($request->id_barang) > 0 ){
        foreach ($request->id_barang as $item => $v) {
              $s = $request->stok[$item];
              $j = $request->jumlah[$item];
              $t = $s - $j;
              $b= array(
                  'stok' => $t
              );
              DB::table('barang')->where('id_barang','=', $request->id_barang[$item])->update($b);
      }
  }
        return redirect('inventory.keluar.grid');
    }
}
