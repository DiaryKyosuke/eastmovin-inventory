@extends('layouts.inventorykeluar')
@section('title','Inventory Barang Keluar')
@section('keluar')
<div class="m-portlet m-portlet--mobile" style="width: 100%;">
	@if(session('sukses'))
	<script type="text/javascript">
	    $(document).ready(function(){
	      	Swal({
			  position: 'top-mid',
			  type: 'success',
			  title: '{{session('sukses')}}',
			  showConfirmButton: true
			})
	    });
  	</script>
	@endif
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Inventory Barang Keluar
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{url('inventory.keluar.form')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>Tambah Data</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_keluar">
			<thead >
				<tr class="text-center">
					<th width="20px">No.</th>
					<th width="40%">Tanggal</th>
					<th width="40%">Keterangan</th>
					<th width="40%">Action</th>
				</tr>
			</thead>
			<tbody>
			@foreach($transaksi as $trans)
				<tr>
					<td>{{++$i}}.</td>
					<td>{{ Carbon\Carbon::parse($trans->tanggal)->formatLocalized('%A, %d %B %Y')}}</td>
					<td>{{$trans->keterangan}}</td>
					<td class="text-center">
						@if($trans->permission == 'terkunci')	
							<a class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="inventory.keluar.{{$trans->id_transaksi}}.view" data-fancybox data-options='{"type" : "iframe", "iframe" : {"preload" : false, "css" : {"width" : "500%"}}}'><i class="fas fa-id-card"></i></a>

							<button class="btn btn-sm btn-outline-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air"><i class="fas fa-key"></i></button>

						@elseif($trans->permission == 'terbuka')
						<form method="post" action="inventory/keluar/{{$trans->id_transaksi}}/permission" enctype="multipart/form-data" class="coba{{$i}}">
							@csrf
							
							<input type="hidden" name="hak_akses" value="terkunci">

							<?php 
								$coba = DB::table('barang')
								->join('detail_transaksi','detail_transaksi.id_barang','=','barang.id_barang')
								->join('transaksi','transaksi.id_transaksi','=','detail_transaksi.id_transaksi')
								->where('detail_transaksi.id_transaksi','=',$trans->id_transaksi)
								->get();
							?>
							<table style="display: none;">
								@foreach($coba as $coba)
								<tr>
									<td>
										<span>Id</span>
										<input style="width: 100%;" name="id_barang[]" type="text" readonly class="form-control mb-2" value="{{$coba->id_barang}}">
									</td>

									<td>
										<span>Stok</span>
                        				<input style="width: 100%;" type="text" name="stok[]" readonly class="form-control mb-2" value="{{$coba->stok}}">
									</td>

									<td>
										<span>Jumlah</span>
										<input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" placeholder="Jumlah" autocomplete="off" value="{{$coba->jumlah}}" onkeyup="angka(this);" required="">
									</td>
								</tr>
								@endforeach
							</table>
							<a class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="inventory.keluar.{{$trans->id_transaksi}}.view" data-fancybox data-options='{"type" : "iframe", "iframe" : {"preload" : false, "css" : {"width" : "500%"}}}'><i class="fas fa-id-card"></i></a>

							<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="inventory.keluar.{{$trans->id_transaksi}}.edit"><i class="fas fa-user-edit"></i></a>

							<a href="inventory/keluar/{{$trans->id_transaksi}}/destroy" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus')"><i class="fas fa-trash-alt"></i></a>

							<a href="#" class="btn btn-sm btn-outline-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air kunci{{$i}}"><i class="fas fa-lock"></i></a>
						</form>
						@else
						@endif
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		<?php foreach ($transaksi as $trans): ?>
			var q{{++$b}} = {{$c++}};
			$('.kunci'+q{{$b}}+'').click(function(){
				Swal.fire({
					type: 'warning',
					title: 'Apakah Anda Yakin Ingin Mengunci Data Tersebut ?',
					text:  'Jika Anda Yakin Data Tidak Bisa Dibuka',
					showCancelButton  :true,
					ConfirmButtonColor:'#3085d6',
					CancelButtonColor :'#d33',
					ConfirmButtonText:'Kunci Data Ini!!',
					CancelButtonText :'Batal Kunci Data Ini!!',
				}).then((result)=> {
					if (result.value) {
						$('.coba'+q{{$b}}+'').submit();
					}
				});
			});	
		<?php endforeach ?>
	});
</script>
@endsection